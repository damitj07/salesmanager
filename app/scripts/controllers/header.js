'use strict';

/**
 * @ngdoc function
 * @name salesManagerApp.controller:headerCtrl
 * @description
 * # headerCtrl
 * Controller of the salesManagerApp for Home page
 */

angular.module('salesManagerApp')
        .controller('headerCtrl', headerFunction);

headerFunction.$inject = ['DataService', '$cookies', '$state'];

function headerFunction(DataService, $cookies, $state) {
    var header = this;
    header.state = $state;
    header.username = $cookies.get('username');
    header.logout = function () {
        return performLogout().then(function (data) {
            if (data) {
                console.log("logout sucessfull");
                $cookies.remove('sessionId');
                $cookies.remove('username');
                $state.go('login');
            }
        });

        function performLogout() {
            return DataService.performLogout($cookies.get('sessionId')).then(function (data) {
                header.message = data;
                return header.message;
            });
        }
    };

}