/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

(function () {
    'use strict';
    angular
            .module('salesManagerApp')
            .run(appRun);
    /* @ngInject */
    appRun.$inject = ['routerHelper'];
    function appRun(routerHelper) {
        routerHelper.configureStates(getStates(), "/login");
    }

    function getStates() {

        return [
            {
                state: 'login',
                config: {
                    url: '/login',
                    views: {
                        '': {templateUrl: 'views/login.html',
                            controller: 'MainCtrl',
                            controllerAs: 'login'
                        }
                    },
                    onEnter: function ($cookies, $state) {
                        console.log("onenterlogin");
                        if (typeof $cookies.get('username') !== "undefined" && typeof $cookies.get('sessionId') !== "undefined") {
                            $state.go('homepage');
                        }
                    }
                }
            },
            {
                state: 'homepage',
                config: {
                    url: '/homepage',
                    views: {
                        '': {templateUrl: 'views/home-page.html',
                            controller: 'HomeCtrl',
                            controllerAs: 'home'},
                        'header': {
                            templateUrl: 'views/header.html',
                            controller: 'headerCtrl',
                            controllerAs: 'header'
                        },
                        'footer': {
                            templateUrl: 'views/footer.html',
                            controller: 'footerCtrl',
                            controllerAs: 'footer'
                        }
                    },
                    onEnter: function ($cookies, $state) {
                        if (typeof $cookies.get('username') === "undefined" && typeof $cookies.get('sessionId') === "undefined") {
                            $state.go('login');
                        }
                    }
                }
            }

        ];
    }

})();