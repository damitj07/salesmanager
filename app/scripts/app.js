'use strict';

/**
 * @ngdoc overview
 * @name salesManagerApp
 * @description
 * # salesManagerApp
 *
 * Main module of the application.
 */
angular
  .module('salesManagerApp', [
    'ngAnimate',
    'ngCookies',
    'ngResource',
    'ngRoute',
    'ngSanitize',
    'ngTouch',
    'ui.router',
    'chart.js',
    'ui.bootstrap'
  ]);
