'use strict';

/**
 * @ngdoc function
 * @name salesManagerApp.controller:MainCtrl
 * @description
 * # MainCtrl
 * Controller of the salesManagerApp
 */
angular.module('salesManagerApp')
        .controller('MainCtrl', LoginFunction);

LoginFunction.$inject = ['$state', 'DataService', '$cookies'];

function LoginFunction($state, DataService, $cookies) {
    var login = this;
    login.login = function (isValid) {
        if (isValid) {
//            console.log("Perform Login.");
            return LoginDone().then(function (data) {
                if (data) {
                    if (data.loginSucceeded) {
                        $state.go('homepage');
                    } else {
                        console.log("Perform Login Failed.");
                        login.loginFailed = true;
                    }
                }
            });
        }
        function LoginDone() {
            return DataService.performLogin(login.username, login.password).then(function (data) {
                login.data = data;
                $cookies.put("sessionId", data.sessionId);
                $cookies.put('username', login.username);
                return login.data;
            });
        }
    };


}
