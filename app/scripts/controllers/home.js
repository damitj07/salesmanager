'use strict';

/**
 * @ngdoc function
 * @name salesManagerApp.controller:HomeCtrl
 * @description
 * # HomeCtrl
 * Controller of the salesManagerApp for Home page
 */
angular.module('salesManagerApp')
        .controller('HomeCtrl', homeFunction);

homeFunction.$inject = ['$state', 'DataService', '$cookies'];

function homeFunction($state, DataService, $cookies) {
    var home = this;
    home.labels = [];
    home.data = [];
    home.tab = 0;
    home.setTab1 = true;
    home.setTab2 = true;
    home.setTab3 = true;
    home.setTab4 = true;

    //BAR Chart
    home.barLabels = [];
    home.barData = [[]];
    home.refreshGetTotalSalesPerMan = function () {
        getTotalSalesPerMan($cookies.get('sessionId'));
    };
    home.refreshGetTotalSalesPerMonth = function () {
        getTotalSalesPerMonth($cookies.get('sessionId'));
    };
    home.refreshGetTopSalesOrder = function () {
        getTopSalesOrder($cookies.get('sessionId'));
    };
    home.refreshGetTopSalesMen = function () {
        getTopSalesMen($cookies.get('sessionId'));
    };

    getTotalSalesPerMan($cookies.get('sessionId'));
    getTotalSalesPerMonth($cookies.get('sessionId'));
    getTopSalesOrder($cookies.get('sessionId'));
    getTopSalesMen($cookies.get('sessionId'));

    function getTotalSalesPerMan(sessionId) {
        return DataService.getTotalSalesPerMan(sessionId).then(function (data) {
            home.totalSalesPerMan = data.data;
//            console.log(home.totalSalesPerMan);
            home.labels = [];
            home.data = [];
            for (var i = 0; i < home.totalSalesPerMan.length; i++) {
                home.labels.push(home.totalSalesPerMan[i][0]);
                home.data.push(parseInt(home.totalSalesPerMan[i][1]));
            }
            return home.totalSalesPerMan;
        });
    }
    function getTotalSalesPerMonth(sessionId) {
        return DataService.getTotalSalesPerMonth(sessionId).then(function (data) {
            home.totalSalesPerMonth = data.data;
            home.barLabels = [];
            home.barData = [[]];
            for (var i = 0; i < home.totalSalesPerMonth.length; i++) {
                home.barLabels.push(home.totalSalesPerMonth[i][0]);
                home.barData[0].push(parseInt(home.totalSalesPerMonth[i][1]));
            }
            return home.totalSalesPerMonth;
        });
    }
    function getTopSalesOrder(sessionId) {
        return DataService.getTopSalesOrder(sessionId).then(function (data) {
            home.topSalesOrder = data.data;
//            console.log(home.topSalesOrder);
            return home.topSalesOrder;
        });
    }
    function getTopSalesMen(sessionId) {
        return DataService.getTopSalesMen(sessionId).then(function (data) {
            home.topSalesMen = data.data;
//            console.log(home.topSalesMen);
            return home.topSalesMen;
        });
    }

}
