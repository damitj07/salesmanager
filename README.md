# Sales Manager Demo App

This project is generated with [yo angular generator](https://github.com/yeoman/generator-angular)
version 0.14.0.This application talks with backend REST API to perform login and other functionality.

## Only Predefined users are allowed to login

Predefined user: **Oswaldo**

Password should be same as user name

If login succeeded response will have session id that will be used in the following requests.


## Build & development
- Extract the backend.zip file to a folder.

- To run backend you should have JDK 1.7 installed on your machine.

- You can run backend with the command java -jar backend.jar.

- Backend will start web server on localhost:8080.

- *npm install* and then *npm install grunt-cli -g* for project setup

- Run `grunt` for building and `grunt serve` for preview.Hit http:localhost:9000/ for application home page.

## Testing

Running `grunt test` will run the unit tests with karma.