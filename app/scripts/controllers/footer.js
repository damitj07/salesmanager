'use strict';

/**
 * @ngdoc function
 * @name salesManagerApp.controller:footerCtrl
 * @description
 * # footerCtrl
 * Controller of the salesManagerApp for Home page
 */

angular.module('salesManagerApp')
        .controller('footerCtrl', footerFunction);

footerFunction.$inject = ['$uibModal', '$state'];

function footerFunction($uibModal, $state) {

    var footer = this;
    footer.state = $state;
    footer.openPrivacyPopUp = function () {
        console.log("exe");
        $uibModal.open({
            animation: true,
            templateUrl: 'views/privacy-policy.html',
            controller: "PrivacyModalCtrl",
            size: 'sm'

        });
    };
    footer.openTermsPopUp = function (size) {
        $uibModal.open({
            animation: true,
            templateUrl: 'views/terms.html',
            controller: 'TermsModalCtrl',
            size: size

        });
    };
    footer.openSupportPopUp = function (size) {
        $uibModal.open({
            animation: true,
            templateUrl: 'views/support.html',
            controller: 'SupportModalCtrl',
            size: size

        });
    };

}
angular.module('salesManagerApp')
        .controller('PrivacyModalCtrl', function ($scope, $uibModalInstance) {

            $scope.dismiss = function () {
                $uibModalInstance.dismiss('cancel');
            };
        });

angular.module('salesManagerApp')
        .controller('TermsModalCtrl', function ($scope, $uibModalInstance) {

            $scope.dismiss = function () {
                $uibModalInstance.dismiss('cancel');
            };
        });

angular.module('salesManagerApp')
        .controller('SupportModalCtrl', function ($scope, $uibModalInstance, $uibModal) {

            $scope.send = function (isvalid) {
                if (isvalid) {
                    $uibModalInstance.dismiss('cancel');
                    $uibModal.open({
                        animation: true,
                        templateUrl: 'views/support-msg.html',
                        controller: 'SupportMsgModalCtrl',
                        size: 'sm'

                    });
                }
            };
            $scope.dismiss = function () {
                $uibModalInstance.dismiss('cancel');
            };
        });

angular.module('salesManagerApp')
        .controller('SupportMsgModalCtrl', function ($scope, $uibModalInstance) {
            $scope.dismiss = function () {
                $uibModalInstance.dismiss('cancel');
            };
        });