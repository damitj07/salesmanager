/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */


(function () {
    "use strict";
    angular
            .module('salesManagerApp')
            .factory('DataService', dataservice);

    dataservice.$inject = ['$http'];

    function dataservice($http) {
        return {
            performLogin: performLogin,
            getTotalSalesPerMan: getTotalSalesPerMan,
            getTotalSalesPerMonth: getTotalSalesPerMonth,
            getTopSalesOrder: getTopSalesOrder,
            getTopSalesMen: getTopSalesMen,
            performLogout: performLogout
        };

        function performLogin(username, password) {
            return $http.get("http://localhost:8080/login?username=" + username + "&password=" + password)
                    .then(loginSuccesfull)
                    .catch(loginFailed);

            function loginSuccesfull(response) {
                return response.data;
            }

            function loginFailed(error) {
                console.log('XHR Failed for login.' + error.data);
            }
        }

        function getTotalSalesPerMan(sessionId) {
            return $http.get("http://localhost:8080/salesmandata?sessionid=" + sessionId)
                    .then(CallSuccesfull)
                    .catch(CallFailed);

            function CallSuccesfull(response) {
                return response.data;
            }

            function CallFailed(error) {
                console.log('XHR Failed .' + error.data);
            }
        }

        function getTotalSalesPerMonth(sessionId) {
            return $http.get("http://localhost:8080/lastyeardata?sessionid=" + sessionId)
                    .then(CallSuccesfull)
                    .catch(CallFailed);

            function CallSuccesfull(response) {
                return response.data;
            }

            function CallFailed(error) {
                console.log('XHR Failed .' + error.data);
            }
        }

        function getTopSalesOrder(sessionId) {
            return $http.get(" http://localhost:8080/topsalesorders?sessionid=" + sessionId)
                    .then(CallSuccesfull)
                    .catch(CallFailed);

            function CallSuccesfull(response) {
                return response.data;
            }

            function CallFailed(error) {
                console.log('XHR Failed .' + error.data);
            }
        }

        function getTopSalesMen(sessionId) {
            return $http.get("http://localhost:8080/topsalesmen?sessionid=" + sessionId)
                    .then(CallSuccesfull)
                    .catch(CallFailed);

            function CallSuccesfull(response) {
                return response.data;
            }

            function CallFailed(error) {
                console.log('XHR Failed .' + error.data);
            }
        }

        function performLogout(sessionId) {

            return $http({
                url: "http://localhost:8080/logout?sessionid=" + sessionId,
                method: 'GET',
                transformResponse: [function (data) {
                        return data;
                    }]
            });
        }

    }
})();